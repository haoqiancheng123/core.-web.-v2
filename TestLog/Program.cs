﻿using ARC.Common.Log;
using Core.FrameWork.Commons.Loging;
using Microsoft.Extensions.Primitives;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace TestLog
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int Count = 100000;

            ////批量写入
            BatchWriteLog(Count);

            ////单次写入
            //WriteLog(Count);

        




            Console.ReadKey();
        }
        /// <summary>
        /// 批量多条  一次写入
        /// </summary>
        static void BatchWriteLog(int Count)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < Count; i++)
            {
                LogLockHelper.WriteLogBatch("Test2", "Test2" + DateTime.Now.ToString("yyyy年MM月dd HH时mm分"), new string[] { "123" + "," }, false);
            }

            sw.Stop();
            TimeSpan ts2 = sw.Elapsed;
            Console.WriteLine("Batch总共花费{0}ms.", ts2.TotalMilliseconds);
        }
        /// <summary>
        /// 单条  单次写入
        /// </summary>
        static void WriteLog(int Count)
        {
            Stopwatch sw_Eq = new Stopwatch();
            sw_Eq.Start();
            for (int i = 0; i < Count; i++)
            {
                LogLockHelper.WriteLog("Test1", "Test1" + DateTime.Now.ToString("yyyy-MM-dd"), new string[] { "123" + "," }, false);

            }

            sw_Eq.Stop();
            TimeSpan tssw_Eq = sw_Eq.Elapsed;
            Console.WriteLine("单条总共花费{0}ms.", tssw_Eq.TotalMilliseconds);
        }
    }
}
