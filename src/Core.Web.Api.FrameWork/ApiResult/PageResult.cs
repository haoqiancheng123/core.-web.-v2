﻿namespace Core.FrameWork.Commons.ApiResult
{
    /// <summary>
    /// 保存分页请求的结果。
    /// </summary>
    /// <typeparam name="T">返回结果集中的POCO类型</typeparam>
    public class PageResult<T> : CommonResult
    {
        /// <summary>
        /// 总页数
        /// </summary>
        public int pageCount { get; set; }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int recordCount { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页数量
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 自定义用户属性。
        /// </summary>
        public object Context { get; set; }
    }
}
