﻿namespace Core.FrameWork.Commons.ApiResult.Model
{
    /// <summary>
    /// 错误代码描述
    /// </summary>
    public static class ErrCode
    {

        /// <summary>
        /// 请求成功
        /// </summary>
        public static string successMsg = "请求成功";
        public static int successCode = 200;

        /// <summary>
        /// 请求失败
        /// </summary>
        public static string errorMsg = "请求失败";
        public static int errorCode = -100;


        /// <summary>
        /// 获取access_token时AppID或AppSecret错误。请开发者认真比对appid和AppSecret的正确性，或查看是否正在为恰当的应用调用接口
        /// </summary>
        public static string err40001Msg = "获取access_token时AppID或AppSecret错误。请开发者认真比对appid和AppSecret的正确性，或查看是否正在为恰当的应用调用接口";
        public static int err40001 = 40001;


        /// <summary>
        /// 调用接口的服务器URL地址不正确，请联系供应商进行设置
        /// </summary>
        public static string err40002Msg = "调用接口的服务器URL地址不正确，请联系供应商进行授权";
        public static int err40002 = 40002;

        /// <summary>
        /// 请确保grant_type字段值为client_credential
        /// </summary>
        public static string err40003Msg = "请确保grant_type字段值为client_credential";
        public static int err40003 = 40003;

        /// <summary>
        /// 不合法的凭证类型
        /// </summary>
        public static string err40004Msg = "不合法的凭证类型";
        public static int err40004 = 40004;

        /// <summary>
        /// 用户令牌accesstoken超时失效
        /// </summary>
        public static string err40005Msg = "用户令牌accesstoken超时失效";
        public static int err40005 = 40005;

        /// <summary>
        /// 您未被授权使用该功能，请重新登录试试或联系管理员进行处理
        /// </summary>
        public static string err40006Msg = "您未被授权使用该功能，请重新登录试试或联系系统管理员进行处理";
        public static int err40006 = 40006;

        /// <summary>
        /// 传递参数出现错误
        /// </summary>
        public static string err40007Msg = "传递参数出现错误";
        public static int err40007 = 40007;

        /// <summary>
        /// 用户未登录或超时
        /// </summary>
        public static string err40008Msg = "用户未登录或超时";
        public static int err40008 = 40008;
        /// <summary>
        /// 程序异常
        /// </summary>
        public static string err40110Msg = "程序异常";
        public static int err40110 = 40110;

        /// <summary>
        /// 更新数据失败
        /// </summary>
        public static string err43001Msg = "新增数据失败";
        public static int err43001 = 43001;

        /// <summary>
        /// 更新数据失败
        /// </summary>
        public static string err43002Msg = "更新数据失败";
        public static int err43002 = 43002;

        /// <summary>
        /// 物理删除数据失败
        /// </summary>
        public static string err43003Msg = "删除数据失败";
        public static int err43003 = 43003;

        /// <summary>
        /// 该用户不存在
        /// </summary>
        public static string err50001Msg = "该用户不存在";
        public static int err50001 = 50001;

        /// <summary>
        /// 该用户已存在
        /// </summary>
        public static string err50002Msg = "用户已存在，请登录或重新注册！";
        public static int err50002 = err50002;

        /// <summary>
        /// 会员注册失败
        /// </summary>
        public static string err50003Msg = "会员注册失败";
        public static int err50003 = 50003;

        /// <summary>
        /// 查询数据不存在
        /// </summary>
        public static string err60001Msg = "查询数据不存在";
        public static int err60001 = 60001;
    }
}
