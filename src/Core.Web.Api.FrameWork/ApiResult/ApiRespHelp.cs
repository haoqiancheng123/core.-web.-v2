﻿using System.Collections.Generic;


namespace Core.FrameWork.Commons.ApiResult
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApiRespHelp
    {


        static ApiRespHelp()
        {

        }


        public static CommonResult<T> GetApiDataList<T>(List<T> t)
        {
            CommonResult<T> result = new CommonResult<T>();
            result.ResData = t;
            result.Success = true;
            result.Code = 200;
            return result;
        }


        public static CommonResult<T> GetApiDataList<T>(int code = 200, string msg = "请求成功", List<T> t = null)
        {
            CommonResult<T> result = new CommonResult<T>();
            result.Code = code;
            result.Msg = msg;
            result.ResData = t;
            result.Success = true;
            return result;
        }



        public static CommonResult<T> GetApiData<T>(T t)
        {
            CommonResult<T> result = new CommonResult<T>();
            result.ResData = t;
            result.Success = true;
            result.Code = 200;
            return result;
        }



        public static CommonResult<T> GetApiData<T>(int code = 200, string msg = "请求成功", T t = null) where T : class
        {
            CommonResult<T> result = new CommonResult<T>();
            result.ResData = t;
            result.Code = code;
            result.Msg = msg;
            result.Success = true;
            return result;
        }
        public static PageResult<T> getApiDataListByPageError<T>(int code = -100, string msg = "请求失败")
        {
            PageResult<T> result = new PageResult<T>();
            result.Code = code;
            result.Msg = msg;
            result.Success = true;
            return result;
        }
        public static PageResult<T> getApiDataListByPage<T>(List<T> list, int recordCount, int pageIndex, int pageSize)
        {
            PageResult<T> result = new PageResult<T>();
            result.ResData = list;
            result.pageCount = (recordCount % pageSize == 0 ? (recordCount / pageSize) : (recordCount / pageSize) + 1);
            result.recordCount = recordCount;
            result.pageIndex = pageIndex;
            result.pageSize = pageSize;
            result.Code = 200;
            result.Success = true;
            return result;
        }
        public static PageResult<T> getApiDataListByPage<T>(object list, int recordCount, int pageIndex, int pageSize)
        {
            PageResult<T> result = new PageResult<T>();
            result.ResData = list;
            result.pageCount = (recordCount % pageSize == 0 ? (recordCount / pageSize) : (recordCount / pageSize) + 1);
            result.recordCount = recordCount;
            result.pageIndex = pageIndex;
            result.pageSize = pageSize;
            result.Success = true;
            result.Code = 200;
            return result;
        }

        public static CommonResult getSuccess(string msg = "请求成功")
        {
            CommonResult result = new CommonResult();
            result.Msg = msg;
            result.Success = true;
            result.Code = 200;
            return result;
        }
        public static CommonResult getError(int code = -100, string msg = "请求失败")
        {
            CommonResult result = new CommonResult();
            result.Code = code;
            result.Msg = msg;
            result.Success = true;
            return result;
        }

        public static CommonResult<T> getError<T>(int code = -100, string msg = "请求失败")
        {
            CommonResult<T> result = new CommonResult<T>();
            result.Code = code;
            result.Msg = msg;
            result.Success = false;
            return result;
        }
    }
}
