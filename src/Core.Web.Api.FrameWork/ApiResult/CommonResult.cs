﻿using System.Runtime.Serialization;

namespace Core.FrameWork.Commons.ApiResult
{
    public class CommonResult
    {
        /// <summary>
        /// BaseResult构造函数
        /// </summary>
        public CommonResult()
        {
            this.Msg = Core.FrameWork.Commons.ApiResult.Model.ErrCode.successMsg;
            this.Code = Core.FrameWork.Commons.ApiResult.Model.ErrCode.successCode;
        }
        /// <summary>
        /// BaseResult构造函数
        /// </summary>
        /// <param name="errmsg">错误消息</param>
        /// <param name="errcode">错误代码</param>
        public CommonResult(string errmsg, int errcode)
        {
            this.Msg = errmsg;
            this.Code = errcode;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="errmsg">错误消息</param>
        /// <param name="success">成功或失败</param>
        /// <param name="errcode">错误代码</param>
        public CommonResult(string errmsg, bool success, int errcode)
        {
            this.Msg = errmsg;
            this.Code = errcode;
            this.Success = success;
        }


        /// <summary>
        /// 错误代码
        /// </summary>
        public int Code
        {
            get; set;
        }

        /// <summary>
        /// 如果不成功，返回的错误描述信息
        /// </summary>
        public string Msg
        {
            get; set;
        }
        /// <summary>
        /// 成功返回true，失败返回false
        /// </summary>
        public bool Success
        {
            get; set;
        }
        /// <summary>
        /// 用来传递的object内容
        /// </summary>
        [DataMember]
        public object? ResData
        {
            get; set;
        }
    }
    /// <summary>
    /// WEBAPI通用返回泛型基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommonResult<T> : CommonResult
    {
        /// <summary>
        /// 回传的结果
        /// </summary>
        public T? Result { get; set; }
    }

}
