﻿using Core.FrameWork.Commons.Hepler.Validate;
using Masuit.Tools;
using System;

namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// 手机号码验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MobileAttribute : AbstractValidateAttribute
    {

        public override ValidateResult Validate(object oValue)
        {
            if (oValue == null)
                return result;

            if (!oValue.ToString().MatchPhoneNumber())
            {
                result.ListErrorMessage.Add($"手机号格式验证失败");
            }
            return result;
        }

    }
}
