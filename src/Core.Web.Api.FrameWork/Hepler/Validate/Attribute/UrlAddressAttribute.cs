﻿using Core.FrameWork.Commons.Hepler.Validate;
using Masuit.Tools;
using System;

namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// Url地址效验验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class UrlAddressAttribute : AbstractValidateAttribute
    {

        public override ValidateResult Validate(object oValue)
        {
            if (oValue == null)
                return result;

            if (!oValue.ToString().MatchUrl())
            {
                result.ListErrorMessage.Add($"Url地址验证失败");
            }
            return result;
        }

    }
}
