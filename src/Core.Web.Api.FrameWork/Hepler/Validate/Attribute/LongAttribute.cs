﻿using Core.FrameWork.Commons.Hepler.Validate;
using System;

namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// 数字长度验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LongAttribute : AbstractValidateAttribute
    {
        private long _Min = 0;
        private long _Max = 0;
        public LongAttribute(long min, long max)
        {
            this._Min = min;
            this._Max = max;
        }

        public override ValidateResult Validate(object oValue)
        {
            bool flag = oValue != null
                && long.TryParse(oValue.ToString(), out long lValue)
                && lValue >= this._Min
                && lValue <= this._Max;
            if (!flag)
            {
                result.ListErrorMessage.Add($"数字大小范围在{this._Min}-{this._Max}之间");
            }
            return result;
        }

    }
}
