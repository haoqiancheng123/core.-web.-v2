﻿using System;
using System.Collections;

namespace Core.FrameWork.Commons.Hepler.Validate.Attribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ListNotNullAttribute : AbstractValidateAttribute
    {
        public override ValidateResult Validate(object oValue)
        {
            if (oValue == null)
                result.ListErrorMessage.Add("是必填字段");
            else
            {
                var list = oValue as IEnumerable;

                int count = 0;

                foreach (var item in list)
                {
                    count++;
                }

                if (count == 0)
                    result.ListErrorMessage.Add("是必填字段");
            }
            return result;
        }
    }
}
