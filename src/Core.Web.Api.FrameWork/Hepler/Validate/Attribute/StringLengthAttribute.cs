﻿using Core.FrameWork.Commons.Hepler.Validate;
using System;

namespace Core.FrameWork.Commons.Helper.Validate.Attribute
{
    /// <summary>
    /// 字符串长度验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class StringLengthAttribute : AbstractValidateAttribute
    {
        private int _Min = 0;
        private int _Max = 0;
        public StringLengthAttribute(int min, int max)
        {
            this._Min = min;
            this._Max = max;
        }

        public override ValidateResult Validate(object oValue)
        {
            bool flag = oValue != null
                && oValue.ToString().Length >= this._Min
                && oValue.ToString().Length <= this._Max;
            if (!flag)
            {
                result.ListErrorMessage.Add($"字符串长度应在{this._Min}-{this._Max}个字符之间");
            }
            return result;
        }

    }
}
