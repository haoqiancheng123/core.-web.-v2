﻿namespace Core.FrameWork.Commons.Hepler.Validate
{
    /// <summary>
    /// 验证Attribute 抽象类
    /// </summary>
    public abstract class AbstractValidateAttribute : System.Attribute
    {
        public ValidateResult result = new ValidateResult();
        public abstract ValidateResult Validate(object oValue);

    }
}
