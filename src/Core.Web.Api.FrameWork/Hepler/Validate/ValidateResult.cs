﻿using System.Collections.Generic;
using System.Linq;

namespace Core.FrameWork.Commons.Hepler.Validate
{
    public class ValidateResult
    {
        public bool Status { get { return ListErrorMessage.Count() == 0; } }

        public string Message
        {
            get
            {
                if (ListErrorMessage.Count > 0)
                {
                    return ListErrorMessage.FirstOrDefault() ?? "";
                }
                return string.Empty;
            }
        }
        public List<string> ListErrorMessage { get; set; } = new List<string>();
    }
}
