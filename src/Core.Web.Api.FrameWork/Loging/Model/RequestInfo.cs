﻿namespace Core.FrameWork.Commons.Loging.Model
{
    /// <summary>
    /// 请求参数实体
    /// </summary>
    public class RequestInfo
    {
        public string Ip { get; set; }
        public string Url { get; set; }
        public string Datetime { get; set; }
        public string Date { get; set; }
        public string Week { get; set; }
    }
}
