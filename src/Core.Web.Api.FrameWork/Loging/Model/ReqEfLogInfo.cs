﻿using System;

namespace Core.FrameWork.Commons.Loging.Model
{
    public class ReqEfLogInfo
    {
        public string Sql { get; set; }

        public DateTime DateTime { get; set; }
    }
}
