﻿using Core.FrameWork.Commons.App;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Loging.Storage
{
    public class FileStorage : ILogStorage
    {
        //private static string _contentRoot = AppCenter.webHostEnvironment.ContentRootPath;
        private static string _contentRoot = @"C:\Users\qianc\source\repos\core.-web.-v2\TestLog";




        public void WriteLog(string rootPath, string filename, string[] dataParas, bool IsHeader = true)
        {
            var path = Path.Combine(_contentRoot, "App_Data/" + rootPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string logFilePath = Path.Combine(path, $@"{filename}.log");

            var now = DateTime.Now;
            string logContent = String.Join("\r\n", dataParas);
            if (IsHeader)
            {
                logContent = (
                   "--------------------------------\r\n" +
                   DateTime.Now + "|\r\n" +
                   String.Join("\r\n", dataParas) + "\r\n"
                   );
            }

            File.AppendAllText(logFilePath, logContent);
        }
        public string ReadLog(string Path, Encoding encode)
        {
            string json = "";

            if (!System.IO.File.Exists(Path))
            {
                return json;
            }

            StreamReader f2 = new StreamReader(Path, encode);
            json = f2.ReadToEnd();
            f2.Close();
            f2.Dispose();

            return json;
        }
    }
}
