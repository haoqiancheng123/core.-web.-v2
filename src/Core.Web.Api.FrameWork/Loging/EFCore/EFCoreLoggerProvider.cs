﻿using Microsoft.Extensions.Logging;

namespace Core.FrameWork.Commons.Loging
{
    public class EFCoreLoggerProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string categoryName) => new EFCoreLogger(categoryName);
        public void Dispose() { }
    }
}
