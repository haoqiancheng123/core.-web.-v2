﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Loging
{
    /// <summary>
    /// 日志存储接口
    /// </summary>
    public interface ILogStorage
    {
        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="filename"></param>
        /// <param name="dataParas"></param>
        /// <param name="IsHeader"></param>
         void WriteLog(string rootPath, string filename, string[] dataParas, bool IsHeader = true);
        /// <summary>
        /// 读取日志
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="encode"></param>
        string ReadLog(string Path, Encoding encode);
    }
}
