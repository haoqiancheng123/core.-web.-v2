﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.FrameWork.Commons.Loging
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ActionDescriptionAttribute : Attribute
    {
        /// <summary>
        /// 操作名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 是否记录响应信息
        /// 在记录列表时不建议启用
        /// </summary>
        public bool LogResponseData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ActionDescriptionAttribute()
        {

        }

        public ActionDescriptionAttribute(string actionName)
        {
            ActionName = actionName;
        }
    }
}
