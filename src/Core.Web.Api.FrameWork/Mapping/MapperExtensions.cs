﻿using Core.FrameWork.Commons.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.FrameWork.Commons.Mapping
{
    /// <summary>
    /// 对象映射扩展操作
    /// </summary>
    public static class MapperExtensions
    {
        /// <summary>
        /// 使用源类型的对象更新目标类型的对象
        /// </summary>
        /// <typeparam name="TSource">源类型</typeparam>
        /// <typeparam name="TDto">目标类型</typeparam>
        /// <param name="source">源对象</param>
        /// <returns>更新后的目标类型对象</returns>
        public static List<TDto> MapTo<TSource, TDto>(this IEnumerable<TSource> source)
        {
            return ExpressionGenericMapper<TSource, TDto>.TransList(source.ToList());
        }
        ///// <summary>
        ///// 使用源类型的对象更新目标类型的对象
        ///// </summary>
        ///// <typeparam name="TSource">源类型</typeparam>
        ///// <typeparam name="TDto">目标类型</typeparam>
        ///// <param name="source">源对象</param>
        ///// <returns>更新后的目标类型对象</returns>
        //public static List<TDto> MapToList<TSource, TDto>(this IList<TSource> source)
        //{
        //    return ExpressionGenericMapper<TSource, TDto>.TransList(source.ToList());
        //}

        /// <summary>
        /// 使用源类型的对象更新目标类型的对象
        /// </summary>
        /// <typeparam name="TSource">源类型</typeparam>
        /// <typeparam name="TDto">目标类型</typeparam>
        /// <param name="source">源对象</param>
        /// <returns>更新后的目标类型对象</returns>
        public static TDto MapTo<TSource, TDto>(this TSource source)
        {
            if (source == null)
                throw new ArgumentNullException("");
            return ExpressionGenericMapper<TSource, TDto>.Trans(source);
        }
        /// <summary>
        /// 使用源类型的对象更新目标类型的对象
        /// </summary>
        /// <typeparam name="TSource">源类型</typeparam>
        /// <typeparam name="TDto">目标类型</typeparam>
        /// <param name="source">源对象</param>
        /// <returns>更新后的目标类型对象</returns>
        public static TDto MapTo<TSource, TDto>(this TSource source, TDto dto)
        {
            if (source == null)
                throw new ArgumentNullException("");
            return ExpressionGenericMapper<TSource, TDto>.Modify(source, dto);
        }



    }
}
