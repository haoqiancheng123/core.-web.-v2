﻿using System;

namespace Core.FrameWork.Commons.ORM.Models
{
    /// <summary>
    /// 软删除
    /// </summary>
    public interface IDeleteAudited
    {
        public int IsDelete { get; set; }
        public DateTime DeleteTime { get; set; }
        public Guid DeleteUser { get; set; }
        public string DeleteUserName { get; set; }
    }
}
