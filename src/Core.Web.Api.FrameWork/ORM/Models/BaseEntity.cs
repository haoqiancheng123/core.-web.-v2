﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.FrameWork.Commons.ORM.Models
{
    public class BaseEntity
    {
        /// <summary>
        /// 获取或设置 编号
        /// </summary>
        [DisplayName("编号")]
        [Key]
        [Column("Id")]
        public virtual Guid Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual DateTime CreateTime { get; set; } = DateTime.Now;//创建时间
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual Guid CreateUserId { get; set; } = Guid.Empty;//创建人
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string CreateUserName { get; set; } = string.Empty;
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual DateTime LastModifyTime { get; set; } = DateTime.Now;
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual Guid LastModifyUserId { get; set; } = Guid.Empty;
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual string LastModifyUserName { get; set; } = string.Empty;
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual bool Enabled { get; set; } = true;


        /// <summary>
        /// 
        /// </summary>
        protected BaseEntity()
        {

        }

    }
}
