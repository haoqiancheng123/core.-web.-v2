﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.FrameWork.Commons.ORM.Models
{
    public class BaseEntityFew
    {
        /// <summary>
        /// 获取或设置 编号
        /// </summary>
        [DisplayName("编号")]
        [Key]
        [Column("Id")]
        public virtual Guid Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public virtual DateTime CreateTime { get; set; } = DateTime.Now;//创建时间
        /// <summary>
        /// 
        /// </summary>
        protected BaseEntityFew()
        {

        }
    }
}
