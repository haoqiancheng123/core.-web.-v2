﻿using Core.FrameWork.Commons.Helper.Validate.Attribute;

namespace Core.FrameWork.Commons.Pages
{
    /// <summary>
    /// 分页实体
    /// </summary>
    public class PagerInfo : Core.FrameWork.Commons.Hepler.Validate.InputModel
    {

        #region 属性变量

        /// <summary>
        /// 获取或设置当前页码
        /// </summary>
        [Required]
        public int CurrentPageIndex { get; set; } = 1;

        /// <summary>
        /// 获取或设置每页显示的记录
        /// </summary>
        [Required]
        public int PageSize { get; set; } = 10;


        #endregion
    }
}
