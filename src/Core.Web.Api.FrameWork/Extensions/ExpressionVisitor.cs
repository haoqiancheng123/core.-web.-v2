﻿using System.Linq.Expressions;

namespace Core.FrameWork.Commons.Extensions
{
    /// <summary>
    /// 建立新表达式
    /// </summary>
    internal class ExpressionVisitor : System.Linq.Expressions.ExpressionVisitor
    {
        public ParameterExpression _NewParameter { get; private set; }
        public ExpressionVisitor(ParameterExpression param)
        {
            this._NewParameter = param;
        }
        public Expression Replace(Expression exp)
        {
            return this.Visit(exp);
        }
        protected override Expression VisitParameter(ParameterExpression node)
        {
            return this._NewParameter;
        }
    }
}
