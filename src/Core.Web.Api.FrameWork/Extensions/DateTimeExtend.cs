﻿using System;

namespace Core.FrameWork.Commons.Extensions
{
    /// <summary>
    /// 时间扩展
    /// </summary>
    public static class DateTimeExtend
    {
        /// <summary>
        /// 求出当前是星期几
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToWeek(this DateTime dateTime)
        {
            string week = string.Empty;
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    week = "周一";
                    break;
                case DayOfWeek.Tuesday:
                    week = "周二";
                    break;
                case DayOfWeek.Wednesday:
                    week = "周三";
                    break;
                case DayOfWeek.Thursday:
                    week = "周四";
                    break;
                case DayOfWeek.Friday:
                    week = "周五";
                    break;
                case DayOfWeek.Saturday:
                    week = "周六";
                    break;
                case DayOfWeek.Sunday:
                    week = "周日";
                    break;
                default:
                    week = "N/A";
                    break;
            }
            return week;
        }
    }
}
