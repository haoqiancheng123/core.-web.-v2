﻿using System;
using System.Collections.Generic;

namespace Core.FrameWork.Commons.Extensions
{
    public static class EnumerableExtension
    {
        /// <summary>
        /// 循环IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="fun"></param>
        /// <returns></returns>
        public static IEnumerable<T> Each<T>(this IEnumerable<T> source, Action<T> fun)
        {
            foreach (T item in source)
            {
                fun(item);
            }
            return source;
        }
    }
}
