﻿using Core.FrameWork.Commons;
using Core.FrameWork.Commons.Cache;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Core.Web.Api.Utils.Init
{
    public static class InitCache
    {
        /// <summary>
        /// 使用缓存
        /// </summary>
        /// <param name="services"></param>
        public static void UseCache(this IServiceCollection services)
        {
            CacheProvider cacheProvider = new CacheProvider
            {
                IsUseRedis = Configs.CacheProvider.UseRedis,
                ConnectionString = Configs.CacheProvider.Redis_ConnectionString,
                InstanceName = Configs.CacheProvider.Redis_InstanceName
            };

            var options = new JsonSerializerOptions();
            options.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
            options.WriteIndented = true;
            options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            options.AllowTrailingCommas = true;
            options.PropertyNameCaseInsensitive = true;                     //忽略大小写
            //判断是否使用Redis，如果不使用 Redis就默认使用 MemoryCache
            if (cacheProvider.IsUseRedis)
            {
                //Use Redis
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = cacheProvider.ConnectionString;
                    options.InstanceName = cacheProvider.InstanceName;
                });
                services.AddSingleton(typeof(ICacheService), new RedisCacheService(new RedisCacheOptions
                {
                    Configuration = cacheProvider.ConnectionString,
                    InstanceName = cacheProvider.InstanceName
                }, options, 0));
                services.Configure<DistributedCacheEntryOptions>(option => option.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5));//设置Redis缓存有效时间为5分钟。
            }
            else
            {
                //Use MemoryCache
                services.AddSingleton<IMemoryCache>(factory =>
                {
                    var cache = new MemoryCache(new MemoryCacheOptions());
                    return cache;
                });
                services.AddSingleton<ICacheService, MemoryCacheService>();
                services.Configure<MemoryCacheEntryOptions>(
                    options => options.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)); //设置MemoryCache缓存有效时间为5分钟
            }
            services.AddTransient<MemoryCacheService>();
            services.AddMemoryCache();// 启用MemoryCache

            services.AddSingleton(cacheProvider);//注册缓存配置
        }
    }
}
