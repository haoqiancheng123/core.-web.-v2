﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Core.Web.Api.Utils.Init
{
    /// <summary>
    /// 配置跨域（CORS）
    /// </summary>
    public static class CorsSetup
    {
        public static void AddCorsSetup(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));
            services.AddCors(c =>
            {
                if (APIConfig.ApiSetting.CorsEnableAllIPs)
                {
                    c.AddPolicy(APIConfig.ApiSetting.CorsPolicyName, policy =>
                    {
                        policy.WithOrigins(APIConfig.ApiSetting.CorsIPs.Split(','));
                        policy.AllowAnyHeader();//Ensures that the policy allows any header.
                        policy.AllowAnyMethod();
                        policy.AllowCredentials();

                    });
                }
                else
                {
                    //允许任意跨域请求
                    c.AddPolicy(APIConfig.ApiSetting.CorsPolicyName, policy =>
                    {

                        policy.SetIsOriginAllowed((host) => true)
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
                }
            });
        }
    }
}
