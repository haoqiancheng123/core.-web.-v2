﻿using Core.Web.Api.DTO.User;
using System.Collections.Generic;
using System.Security.Claims;

namespace Core.Web.Api.Utils.HttpContextUser
{
    public interface IHttpContextUser
    {
        /// <summary>
        /// 获取Token 解析后的Claim 数据
        /// </summary>
        /// <returns></returns>
        public List<string> GetUserInfoFromToken(string ClaimType);
        /// <summary>
        /// 获取用户数据
        /// </summary>
        /// <returns></returns>
        public TokenUserDto GetUserInfo();
        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        string GetToken();

        IEnumerable<Claim> GetClaimsIdentity(string ClaimType);

        bool IsAuthenticated();
    }

}
