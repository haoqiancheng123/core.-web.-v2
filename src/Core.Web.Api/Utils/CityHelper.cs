﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Core.Web.Api.Utils
{
    public class CityHelper
    {
        static CityHelper()
        {
            city_Cn = InitCityJson();
        }
        public static City_cn GetCity => city_Cn;

        private static City_cn city_Cn;
        private static City_cn InitCityJson()
        {
            try
            {
                using (StreamReader sr = new StreamReader(@"city.json", Encoding.UTF8))
                {

                    string json = sr.ReadToEnd();
                    return JsonSerializer.Deserialize<City_cn>(json);

                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
    public class City_cn
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<City_cn> children { get; set; }
    }
}
