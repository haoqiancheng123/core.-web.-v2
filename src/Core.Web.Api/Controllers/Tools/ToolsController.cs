﻿using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Commons.Email.Core;
using Core.FrameWork.Commons.Extensions;
using Core.FrameWork.Commons.Loging;
using Core.FrameWork.Models;
using Core.Web.Api.Utils;
using Core.Web.Api.Utils.HttpContextUser;
using Jaina.EventBus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Web.Api.Controllers.Tools
{
    /// <summary>
    /// 工具控制器
    /// </summary>
    public class ToolsController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationContext"></param>
        /// <param name="httpContextUser"></param>
        public ToolsController(ApplicationContext applicationContext, IHttpContextUser httpContextUser) : base(applicationContext, httpContextUser)
        {
        }
        /// <summary>
        /// 获取省-市-县(如果不全请替换项目city.json文件)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public CommonResult GetArea() => ApiRespHelp.GetApiData(CityHelper.GetCity);
        /// <summary>
        /// 获取 系统配置 （测试）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public CommonResult GetApplicationConfig()
        {
            return ApiRespHelp.GetApiData(APIConfig.AppSetting.DownloadUrl);
        }
        ///// <summary>
        ///// 获取日志
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public CommonResult GetLogging(DateTime dateTime)
        //{
        //    return ApiRespHelp.GetApiDataList(LogLockHelper.log(dateTime));
        //}

    }
}
