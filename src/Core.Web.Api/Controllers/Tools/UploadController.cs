﻿using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Commons.Extensions;
using Core.FrameWork.Commons.Storage;
using Core.FrameWork.Models;
using Core.Web.Api.OutPutModel.Sys;
using Core.Web.Api.Utils.HttpContextUser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Web.Api.Controllers.Tools
{
    /// <summary>
    /// 上传文件控制器
    /// </summary>
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UploadController : BaseController
    {
        public UploadController(ApplicationContext applicationContext, IHttpContextUser httpContextUser) : base(applicationContext, httpContextUser)
        {
        }

        /// <summary>
        /// 上传存储
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<CommonResult> UploadBlobAsync([FromForm] IFormFile file, [FromForm] UploadFileType type = UploadFileType.File)
        {
            List<UploadResult> uploadResults = new List<UploadResult>();

            if (Request.Form == null)
                return ApiRespHelp.getError(-100, "错误");

            var files = Request.Form.Files;

            if (files == null)
                return ApiRespHelp.getError(-100, "请选择上传文件");
            if (files.Count == 0)
                return ApiRespHelp.getError(-100, "请选择上传文件");

            foreach (var fileItem in files)
            {
                if (fileItem == null)
                    return ApiRespHelp.getError(-100, "文件为空");
                if (string.IsNullOrEmpty(fileItem.FileName))
                    return ApiRespHelp.getError(-100, "必须有文件名");


                string nameStr = Guid.NewGuid().ToString();
                string newFileName = string.Format("{0}{1}", nameStr, GetFileExtension(fileItem.FileName));

                var bytes = await fileItem.OpenReadStream().ToBytesAsync();
                var fileurl = await UploadHelp.Execute(bytes, newFileName, type);


                UploadResult uploadResult = new UploadResult()
                {
                    FileName = newFileName,
                    FileUrl = fileurl
                };
                uploadResults.Add(uploadResult);

            }

            return ApiRespHelp.GetApiDataList(200, "上传成功", uploadResults);


        }
        /// <summary>
        /// 上传本地文件
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CommonResult> UploadLocal([FromForm] IFormFile file, [FromForm] UploadFileType type = UploadFileType.File)
        {
            List<UploadResult> uploadResults = new List<UploadResult>();

            if (Request.Form == null)
                return ApiRespHelp.getError(-100, "错误");

            var files = Request.Form.Files;

            if (files == null)
                return ApiRespHelp.getError(-100, "请选择上传文件");
            if (files.Count == 0)
                return ApiRespHelp.getError(-100, "请选择上传文件");

            foreach (var fileItem in files)
            {
                if (fileItem == null)
                    return ApiRespHelp.getError(-100, "文件为空");
                if (string.IsNullOrEmpty(fileItem.FileName))
                    return ApiRespHelp.getError(-100, "必须有文件名");


                string nameStr = Guid.NewGuid().ToString();
                string newFileName = string.Format("{0}{1}", nameStr, GetFileExtension(fileItem.FileName));

                var bytes = await fileItem.OpenReadStream().ToBytesAsync();
                var fileurl = await UploadHelp.Execute(bytes, newFileName, type, UploadFunEnum.location);


                UploadResult uploadResult = new UploadResult()
                {
                    FileName = newFileName,
                    FileUrl = fileurl
                };
                uploadResults.Add(uploadResult);

            }

            return ApiRespHelp.GetApiDataList(200, "上传成功", uploadResults);
        }
        #region Helper
        /// <summary>
        /// 获取文件后缀
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected string GetFileExtension(string fileName)
        {
            if (fileName.IndexOf(".") > 0)
            {
                return fileName.Substring(fileName.LastIndexOf("."));
            }
            return "";
        }
    }
    #endregion

}

