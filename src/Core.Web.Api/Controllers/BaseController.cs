﻿using Core.FrameWork.Models;
using Core.Web.Api.Utils.HttpContextUser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Core.Web.Api.Controllers
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// 数据操作对象
        /// </summary>
        protected readonly ApplicationContext _applicationContext;
        /// <summary>
        /// 获取用户信息
        /// </summary>
        protected readonly IHttpContextUser _httpContextUser;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="applicationContext"></param>
        /// <param name="httpContextUser"></param>
        public BaseController(ApplicationContext applicationContext, IHttpContextUser httpContextUser)
        {
            _applicationContext = applicationContext;
            _httpContextUser = httpContextUser;
        }
        /// <summary>
        /// 当前时间
        /// </summary>
        protected static DateTime NowTime
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
