﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Core.Web.Api.Swagger
{
    public class ApplyTagDescriptions : IDocumentFilter
    {
        /// <summary>
        /// swagger汉化标签
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new List<OpenApiTag>
            {
                new OpenApiTag { Name = "Tools", Description = "工具" },
                new OpenApiTag { Name = "Upload", Description = "文件上传" },
            };
        }
    }
}
