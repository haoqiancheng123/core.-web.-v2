﻿namespace Core.Web.Api.OutPutModel.Sys
{
    /// <summary>
    /// 上传问卷DTO
    /// </summary>
    public class UploadResult
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件地址
        /// </summary>
        public string FileUrl { get; set; }
    }
}
