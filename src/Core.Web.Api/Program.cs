using Core.FrameWork.Commons.Loging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NLog.Web;
using System;
using LogLevel = NLog.LogLevel;

namespace Core.Web.Api
{
    /// <summary>
    /// 程序入口
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            try
            {
                //确保NLog.config中连接字符串与appsettings.json中同步
                NLogUtil.EnsureNlogConfig("NLog.config");
                
                host.Run();

       
                //其他项目启动时需要做的事情
                NLogUtil.WriteAll(LogLevel.Trace, LogType.Web, "接口启动", "接口启动成功");
            }
            catch (Exception ex)
            {
                //使用nlog写到本地日志文件（万一数据库没创建/连接成功）
                NLogUtil.WriteFileLog(LogLevel.Error, LogType.ApiRequest, "接口启动", "初始化数据异常", ex);
                throw;
            }


        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureLogging(logging =>
            {

            })
            .UseNLog()//使用Nlog
            .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration((context, config) =>
                    {
                        config.AddJsonFile($"appsettings{context.HostingEnvironment.EnvironmentName}.json", true, true);//动态更新配置文件
                    });
                    webBuilder.UseStartup<Startup>();
                });
    }
}
