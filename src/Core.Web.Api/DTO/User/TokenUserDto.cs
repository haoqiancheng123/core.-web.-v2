﻿using System;

namespace Core.Web.Api.DTO.User
{
    public class TokenUserDto
    {
        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 微信唯一标识符
        /// </summary>
        public string OpenId { get; set; }
    }
}
