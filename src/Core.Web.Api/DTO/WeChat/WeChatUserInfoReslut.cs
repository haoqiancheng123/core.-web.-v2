﻿using System.Collections.Generic;

namespace Core.Web.Api.DTO.WeChat
{
    /// <summary>
    /// 获取微信用户信息  DTO
    /// </summary>
    public class WeChatUserInfoReslut
    {
        /// <summary>
        /// 
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string nickname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string headimgurl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> privilege { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string unionid { get; set; }
    }
}
