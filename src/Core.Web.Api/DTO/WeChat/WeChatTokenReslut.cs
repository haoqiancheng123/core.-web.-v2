﻿namespace Core.Web.Api.DTO.WeChat
{
    /// <summary>
    /// 获取微信Token  DTO
    /// </summary>
    public class WeChatTokenReslut
    {
        /// <summary>
        /// 
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int expires_in { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string refresh_token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string scope { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string unionid { get; set; }
    }
}
