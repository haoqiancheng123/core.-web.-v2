﻿using Microsoft.EntityFrameworkCore;

namespace Core.FrameWork.Models
{

    public class AdminContext : DbContext
    {
        public AdminContext(DbContextOptions<AdminContext> options)
       : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
     
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
         
        }
    }
}
