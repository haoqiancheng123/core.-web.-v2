﻿using Core.FrameWork.Commons;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Extensions;
using System.Collections.Generic;

namespace Core.AdminUI.V2
{
    /// <summary>
    /// 后台管理配置
    /// </summary>
    public class AdminConfig : Configs
    {
        static AdminConfig()
        {
            AdminSetting = new AdminSetting();
            RoleMapConfig = new List<RoleMapConfig>();

        }
        public static AdminSetting AdminSetting { get; set; }

        public static List<RoleMapConfig> RoleMapConfig { get; set; }
    }
    /// <summary>
    /// API全局配置
    /// </summary>
    public class AdminSetting : AppSetting
    {
        /// <summary>
        /// 万能验证码
        /// </summary>
        public string OverCode => AppCenter.Configuration.GetSection("AppSetting")["OverCode"];
        /// <summary>
        /// 是否开启记录IP
        /// </summary>
        public bool IPLog => AppCenter.Configuration.GetSection("AppSetting")["IPLog"].ToBool();
        /// <summary>
        /// 是否记录用户行为日志
        /// </summary>
        public bool RecordUsersLogs => AppCenter.Configuration.GetSection("AppSetting")["RecordUsersLogs"].ToBool();


    }
    /// <summary>
    /// 角色权限
    /// </summary>
    public class RoleMapConfig
    {
        public string url { get; set; }
        public string role { get; set; }
    }
}
