using Core.AdminUI.V2.Filter;
using Core.AdminUI.V2.Middleware;
using Core.AdminUI.V2.Utils;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Loging;
using Core.FrameWork.Models;
using Core.FrameWork.Service.User;
using Core.FrameWork.Service.ViewBaseTree;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;

namespace Core.AdminUI.V2
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            Configuration = configuration;
            Environment = webHostEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<RecordAccessActionFilterAsync>();
            services.AddMvc(s =>
            {
                s.Filters.AddService<RecordAccessActionFilterAsync>();
            }).AddNewtonsoftJson(option =>
            {

                option.SerializerSettings.ContractResolver = new DefaultContractResolver();
                option.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";//有了这句话，字段上可以不用特性
            }).AddRazorRuntimeCompilation(); //允许动态编译;
            //初始化全局配置
            AppCenter.Services = services;
            AppCenter.Configuration = Configuration;
            AppCenter.webHostEnvironment = Environment;

            //初始化一些配置
            StartApp.Init(Configuration);


            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(options =>
            {
                options.LoginPath = "/SSO/Login";
                options.ClaimsIssuer = "Cookie";
            });

            services.InitUploadFile();
            services.AddInitHangfire();
            services.AddHttpContextAccessor();

            services.AddDbContext<AdminContext>(options =>
            {
                var loggerFactory = new LoggerFactory();
                loggerFactory.AddProvider(new EFCoreLoggerProvider());
                options.UseMySql(AdminConfig.DbConnections.ConnectionString, ServerVersion.Parse("8.0.1")).UseLoggerFactory(loggerFactory);
            });
            services.UseCache();//缓存中间件
            services.AddTransient<IViewTreeConfigService, AdminViewTreeConfigService>();//数据数配置中心

            services.AddScoped<UserService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true,
                DefaultContentType = "yml"
            });

            app.UseRouting();

            app.UseMiddleware<IPLogHandlerMiddleware>();//配置IP记录

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
