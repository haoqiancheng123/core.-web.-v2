﻿using Core.AdminUI.V2.Utils;
using Core.FrameWork.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Core.AdminUI.V2.Controllers.Base
{
    [Route("/[Controller]/[Action]")]
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
    public class BaseController : Controller
    {
        protected AdminContext _adminContext;
        public BaseController(AdminContext adminContext)
        {
            _adminContext = adminContext;
        }

        public CurrentUser CurrentUser
        {
            get
            {
                return HttpContext.GetCurrentUser();
            }
        }
    }
}
