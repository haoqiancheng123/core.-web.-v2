﻿using Core.AdminUI.V2.Controllers.Base;
using Core.AdminUI.V2.Models.AdminRecords;
using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Commons.Loging;
using Core.FrameWork.Commons.Mapping;
using Core.FrameWork.Models;
using Core.FrameWork.Models.Models.System;
using Masuit.Tools.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Core.AdminUI.V2.Controllers
{
    /// <summary>
    /// 后台日志管理
    /// </summary>
    public class AdminRecordLogsController : BaseController
    {
        public AdminRecordLogsController(AdminContext adminContext) : base(adminContext)
        {
        }
        #region 视图
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ApiIndex()
        {
            return View();
        }
        public IActionResult ApiInfo(int Id)
        {
            var info = _adminContext.SysNLogRecords.AsNoTracking().FirstOrDefault(s => s.Id.Equals(Id));
            if (info == null)
            {
                return Redirect(nameof(Index));
            }

            return View(info);
        }
        public IActionResult Info(string Id)
        {
            //验证参数是否合法
            if (!Guid.TryParse(Id, out var GId))
            {
                return Redirect(nameof(Index));
            }

            var info = _adminContext.SysNLogAdminRecords.AsNoTracking().FirstOrDefault(s => s.Id.Equals(GId));
            if (info == null)
            {
                return Redirect(nameof(Index));
            }

            return View(info.MapTo<SysNLogAdminRecords, RespAdminRecordsLogs>());
        }
        #endregion
        #region 接口
        /// <summary>
        /// 获取系统日志记录
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [NotLog]
        public PageResult<RespAdminRecordsLogs> GetPageList(int page = 1, int limit = 10, string name = "", string actionName = "", string beginTime = "", string endTime = "", int beginResponseTime = 0, int endResponseTime = 0)
        {
            Expression<Func<SysNLogAdminRecords, bool>> exp = m => true;
            //拼接条件
            if (!string.IsNullOrEmpty(name))
            {
                exp = exp.And(s => s.UserName.Contains(name));
            }
            if (!string.IsNullOrEmpty(actionName))
            {
                exp = exp.And(s => s.ActionName.Contains(actionName));
            }
            if (!string.IsNullOrEmpty(beginTime))
            {
                if (DateTime.TryParse(beginTime, out var beginTimeD))
                {
                    exp = exp.And(s => s.CreateTime >= beginTimeD);
                }
            }
            if (!string.IsNullOrEmpty(endTime))
            {
                if (DateTime.TryParse(endTime, out var endTimeD))
                {
                    exp = exp.And(s => s.CreateTime <= endTimeD);
                }

            }

            if (beginResponseTime != 0)
            {
                exp = exp.And(s => s.ResponseTime >= beginResponseTime);
            }
            if (endResponseTime != 0)
            {
                exp = exp.And(s => s.ResponseTime <= endResponseTime);
            }

            var list = _adminContext.SysNLogAdminRecords.AsNoTracking().Where(exp).OrderByDescending(m => m.CreateTime)
            .Skip((page - 1) * limit)
            .Take(limit).ToList();

            int count = _adminContext.SysNLogAdminRecords.Count(exp);

            return ApiRespHelp.getApiDataListByPage(list.MapTo<SysNLogAdminRecords, RespAdminRecordsLogs>(), count, page, limit);

        }
        /// <summary>
        /// 获取接口日志记录
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [NotLog]
        public PageResult<SysNLogRecords> GetApiPageList(int page = 1, int limit = 10, string netRequestMethod = "", string logTitle = "", string message = "", string exception = "", string beginTime = "", string endTime = "")
        {
            Expression<Func<SysNLogRecords, bool>> exp = m => true;
            //拼接条件
            if (!string.IsNullOrEmpty(netRequestMethod))
            {
                exp = exp.And(s => s.NetRequestMethod.Contains(netRequestMethod));
            }
            if (!string.IsNullOrEmpty(logTitle))
            {
                exp = exp.And(s => s.LogTitle.Contains(logTitle));
            }
            if (!string.IsNullOrEmpty(message))
            {
                exp = exp.And(s => s.Message.Contains(message));
            }
            if (!string.IsNullOrEmpty(exception))
            {
                exp = exp.And(s => s.Exception.Contains(exception));
            }
            if (!string.IsNullOrEmpty(beginTime))
            {
                if (DateTime.TryParse(beginTime, out var beginTimeD))
                {
                    exp = exp.And(s => s.LogDate >= beginTimeD);
                }
            }
            if (!string.IsNullOrEmpty(endTime))
            {
                if (DateTime.TryParse(endTime, out var endTimeD))
                {
                    exp = exp.And(s => s.LogDate <= endTimeD);
                }

            }

            var list = _adminContext.SysNLogRecords.AsNoTracking().Where(exp).OrderByDescending(m => m.LogDate)
            .Skip((page - 1) * limit)
            .Take(limit).ToList();

            int count = _adminContext.SysNLogRecords.Count(exp);

            return ApiRespHelp.getApiDataListByPage(list, count, page, limit);

        }
        #endregion

    }
}
