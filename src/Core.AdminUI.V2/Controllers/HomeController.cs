﻿using Core.AdminUI.V2.Controllers.Base;
using Core.AdminUI.V2.Models;
using Core.AdminUI.V2.Models.Menu;
using Core.FrameWork.Commons.App;
using Core.FrameWork.Commons.Loging;
using Core.FrameWork.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Core.AdminUI.V2.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(AdminContext adminContext) : base(adminContext)
        {
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionDescriptionAttribute("获取菜单数据", LogResponseData = false)]
        public List<MenuInfo> GetMenu()
        {
            using (StreamReader sr = new StreamReader(AppCenter.webHostEnvironment.ContentRootPath + @"/wwwroot/admin/data/menu.json", Encoding.UTF8))
            {
                string json = sr.ReadToEnd();
                var menu = JsonConvert.DeserializeObject<List<MenuInfo>>(json).ToList();

                List<MenuInfo> menuList = InitMumus(menu);

                //foreach (var menu1 in menu)
                //{
                //    foreach (var menu2 in menu1.role.Split(','))
                //    {
                //        if (CurrentUser.Roles.Contains(menu2))
                //        {
                //            menuList.Add(menu1);

                //            foreach (var menu3 in menu1.children)
                //            {
                //                foreach (var menu4 in menu3.role.Split(','))
                //                {
                //                    if (CurrentUser.Roles.Contains(menu2))
                //                    {
                //                        menu1.children.Add(menu3);
                //                    }
                //                }
                //            }
                //            continue;

                //        }

                //    }
                return menuList;
            }
        }
        public List<MenuInfo> InitMumus(List<MenuInfo> menuInfos)
        {
            var list = new List<MenuInfo>();

            if (menuInfos.Count == 0)
                return list;

            foreach (var item in menuInfos)
            {
                foreach (var menu2 in item.role.Split(','))
                {
                    if (CurrentUser.Roles.Contains(menu2))
                    {
                        list.Add(item);
                        break;
                    }
                  
                
                }
                item.children = InitMumus(item.children);
            }

            return list;
        }
    }
}
