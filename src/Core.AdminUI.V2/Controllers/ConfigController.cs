﻿using Core.AdminUI.V2.Controllers.Base;
using Core.AdminUI.V2.Models.Config;
using Core.FrameWork.Commons.ApiResult;
using Core.FrameWork.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Core.AdminUI.V2.Controllers
{
    public class ConfigController : BaseController
    {
        /// <summary>
        /// 控制器
        /// </summary>
        /// <param name="adminContext"></param>
        public ConfigController(AdminContext adminContext) : base(adminContext)
        {
        }
        /// <summary>
        /// 金币配置页面
        /// </summary>
        /// <returns></returns>
        public IActionResult MoneyIndex()
        {
            var sysordermoneyrule = _adminContext.SysOrderRule.Include(s => s.SysOrderMoneyRule.OrderBy(s => s.OrderServiceTime)).OrderBy(s => s.ClerkLevelType).ThenBy(s => s.OrderTypeEnum).AsEnumerable().GroupBy(s => s.ClerkLevelType).ToList();
            return View(sysordermoneyrule);
        }
        [HttpPost]
        public CommonResult UpdateMoney([FromBody]List<ReqMoney> reqMoney)
        {
            var Ids = reqMoney.Select(s => s.Id).ToList();

            var list = _adminContext.SysOrderMoneyRule.Where(s => Ids.Contains(s.Id)).ToList();

            foreach (var item in list)
            {
                var moneyInfo = reqMoney.FirstOrDefault(s => s.Id.Equals(item.Id));

                item.Money = moneyInfo.Money;
            }

            if (_adminContext.SaveChanges() == 0)
            {
                return ApiRespHelp.getError(-100, "操作失败");
            }
            return ApiRespHelp.getSuccess("操作成功");
        }
    }
}
