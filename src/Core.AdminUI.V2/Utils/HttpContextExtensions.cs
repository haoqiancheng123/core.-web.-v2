﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;
namespace Core.AdminUI.V2.Utils
{
    public static class HttpContextExtensions
    {
        public static CurrentUser GetCurrentUser(this HttpContext httpContext)
        {
            CurrentUser currentUser = new CurrentUser();
            if (httpContext.User.Claims.Count() > 0)
            {
                var roles = httpContext.User.Claims.Where(m => m.Type == ClaimTypes.Role).Select(m => m.Value).ToList();
                currentUser.Id = httpContext.User.Claims.FirstOrDefault(m => m.Type == ClaimTypes.NameIdentifier).Value;
                currentUser.Name = httpContext.User.Identity.Name;
                currentUser.Roles = roles;
                currentUser.Email = httpContext.User.Claims.FirstOrDefault(m => m.Type == ClaimTypes.Email).Value;
            }
            return currentUser;
        }
    }
}
