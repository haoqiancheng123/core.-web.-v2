﻿using Core.FrameWork.Commons.Storage;
using Microsoft.Extensions.Configuration;

namespace Core.AdminUI.V2.Utils
{
    public class StartApp
    {
        public static void Init(IConfiguration configuration)
        {

            #region 初始化文件上传委托
            ////Azure blob方式
            //UploadHelp.Init((bytes, filename, fileType) =>
            //{
            //    var type = (BlobFileType)(int)fileType;
            //    return bytes.Save(filename, type);
            //}, UploadFunEnum.blob);
            ////本地保存
            //UploadHelp.Init((bytes, filename, fileType) =>
            //{
            //    string fileDir = Path.Combine($"Upload/{fileType}/");
            //    if (!Directory.Exists(fileDir))
            //    {
            //        Directory.CreateDirectory(fileDir);
            //    }
            //    string resourcepath = fileDir + filename;
            //    System.IO.File.WriteAllBytes(resourcepath, bytes);
            //    string imageUrl = AppConfig.DownloadUrl + resourcepath;
            //    /*return imageUrl;*/
            //}, UploadFunEnum.location);

            //#region 本地分片上传及合并初始化
            ////本地分片上传
            ////UploadHelp.Init((bytes, fileDir, fileType) =>
            ////{
            ////    fileDir = Path.Combine($"Upload/temp/{fileType}/{fileDir}");
            ////    if (!Directory.Exists(fileDir))
            ////    {
            ////        Directory.CreateDirectory(fileDir);
            ////    }
            ////    var tempName = Guid.NewGuid().ToString();
            ////    var resourcePath = Path.Combine(fileDir, tempName);
            ////    File.WriteAllBytes(resourcePath, bytes);
            ////    return tempName;
            ////}, UploadFunEnum.LocationBlack);
            //////本地分片上传合并
            ////UploadHelp.InitMerge((blacks, fileName, fileDir, fileType) =>
            ////{
            ////    fileDir = Path.Combine($"Upload/temp/{fileType}/{fileDir}");
            ////    var dir = $"Upload/{fileType}/";
            ////    if (!Directory.Exists(dir))
            ////    {
            ////        Directory.CreateDirectory(dir);
            ////    }
            ////    var resourceFile = Path.Combine(dir, fileName);
            ////    var fs = new FileStream(resourceFile, FileMode.Create);
            ////    foreach (var black in blacks)
            ////    {
            ////        var p = Path.Combine(fileDir, black);
            ////        var bytes = File.ReadAllBytes(p);
            ////        fs.Write(bytes, 0, bytes.Length);
            ////        File.Delete(p);
            ////    }
            ////    fs.Flush();
            ////    fs.Close();
            ////    Directory.Delete(fileDir);

            ////    return AppConfig.DownloadUrl + resourceFile;
            ////}, UploadFunEnum.LocationBlack);

            //#endregion

            //#region 本地分片上传及合并初始化

            ////本地分片上传
            //UploadHelp.Init((bytes, fileDir, fileType) =>
            //{
            //    fileDir = Path.Combine($"wwwroot/Upload/temp/{fileType}/{fileDir}");
            //    if (!Directory.Exists(fileDir))
            //    {
            //        Directory.CreateDirectory(fileDir);
            //    }
            //    var tempName = Guid.NewGuid().ToString();
            //    var resourcePath = Path.Combine(fileDir, tempName);
            //    File.WriteAllBytes(resourcePath, bytes);
            //    return tempName;
            //}, UploadFunEnum.LocationBlack);
            ////本地分片上传合并
            //UploadHelp.InitMerge((blacks, fileName, fileDir, fileType) =>
            //{
            //    fileDir = Path.Combine($"wwwroot/Upload/temp/{fileType}/{fileDir}");
            //    var dir = $"wwwroot/Upload/{fileType}/";
            //    if (!Directory.Exists(dir))
            //    {
            //        Directory.CreateDirectory(dir);
            //    }

            //    var pageSize = 5;
            //    var maxPage = (int)Math.Ceiling(blacks.Count * 1.0 / pageSize);
            //    for (int i = 0; i < maxPage; i++)
            //    {
            //        var resourceFile = Path.Combine(dir, fileName);
            //        var fs = new FileStream(resourceFile, FileMode.Append);
            //        var blackList = blacks.Skip(i * pageSize).Take(pageSize).ToList();
            //        foreach (var black in blackList)
            //        {
            //            var p = Path.Combine(fileDir, black);
            //            var bytes = File.ReadAllBytes(p);
            //            fs.Write(bytes, 0, bytes.Length);
            //            File.Delete(p);
            //        }
            //        fs.Flush();
            //        fs.Close();
            //    }
            //    Directory.Delete(fileDir, true);
            //    return AppConfig.DownloadUrl + $"Upload/{fileType}/" + fileName;
            //}, UploadFunEnum.LocationBlack);

            #endregion 本地分片上传及合并初始化

            //设置默认上传保存方式
            UploadHelp.InitDefaultUpload(AdminConfig.AdminSetting.IsBlob ? UploadFunEnum.blob : UploadFunEnum.location);



            //log4net初始化
            //LogFactory.Init(LogManager.CreateRepository("rollingAppender"));
        }
    }
}
