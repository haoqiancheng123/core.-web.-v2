﻿using Core.FrameWork.Commons.Storage;
using Core.FrameWork.Commons.Storage.AliyunOSS;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace Core.AdminUI.V2.Utils
{
    /// <summary>
    /// 初始化工具
    /// </summary>
    public static class InitTools
    {
        /// <summary>
        /// 配置上传文件
        /// </summary>
        /// <param name="services"></param>
        public static void InitUploadFile(this IServiceCollection services)
        {
            // OSS 方式
            UploadHelp.Init((bytes, filename, fileType) =>
           {
               var type = (BlobFileType)(int)fileType;
               return bytes.SaveFile(filename, type);
           }, UploadFunEnum.blob);
            //本地保存
            UploadHelp.Init(async (bytes, filename, fileType) =>
            {
                string fileDir = Path.Combine($"Upload/{fileType.ToString()}/");
                if (!Directory.Exists(fileDir))
                {
                    Directory.CreateDirectory(fileDir);
                }
                string resourcepath = fileDir + filename;
                await System.IO.File.WriteAllBytesAsync(resourcepath, bytes);
                string imageUrl = AdminConfig.AppSetting.DownloadUrl + resourcepath;
                return imageUrl;
            }, UploadFunEnum.location);
            //设置默认上传保存方式
            UploadHelp.InitDefaultUpload(AdminConfig.AppSetting.IsBlob ? UploadFunEnum.blob : UploadFunEnum.location);
        }
    }
}
