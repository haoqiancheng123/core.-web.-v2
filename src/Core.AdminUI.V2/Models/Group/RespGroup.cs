﻿using Core.FrameWork.Models.Enum.Common;
using Core.FrameWork.Models.Enum.User;
using Core.FrameWork.Models.User;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.AdminUI.V2.Models.Clerk
{
    public class RespGroup
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public Guid Id { get; set; } = Guid.Empty;
        /// <summary>
        /// 用户组
        /// </summary>
        public string GroupName { get; set; } = string.Empty;
        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 最后一次修改时间
        /// </summary>
        public DateTime LastModifyTime { get; set; }

    }
}
