﻿using Core.FrameWork.Models.Enum.Common;
using System;

namespace Core.AdminUI.V2.Models.User
{
    public class RespSimpleUser
    {
        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public int UserNumber { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Name { get; set; } 
        /// <summary>
        /// 头像  ;分割
        /// </summary>
        public string ImageUrl { get; set; } 
        /// <summary>
        /// 微信唯一标识
        /// </summary>
        public string OpenId { get; set; } 
        /// <summary>
        /// 微信号
        /// </summary>
        public string WeChatAccount { get; set; }
        /// <summary>
        /// 性别  1：男 0 ：女
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 经验值
        /// </summary>
        public int Experience { get; set; }
        /// <summary>
        /// 金币
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// 邀请获得的奖金
        /// </summary>
        public decimal InvitationMoney { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// 星座
        /// </summary>
        public string Constellation { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 职业
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// 邀请码
        /// </summary>
        public string InvitationCode { get; set; } 
        /// <summary>
        /// 用户等级
        /// </summary>
        public string VipName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime LastModifyTime { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public  bool Enabled { get; set; } 
    }
}
