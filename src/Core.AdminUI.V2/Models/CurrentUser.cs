﻿using System.Collections.Generic;

namespace Core.AdminUI.V2
{
    public class CurrentUser
    {
        /// <summary>
        /// 登录人用户Id
        /// </summary>
        public string Id { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;
        public string Mobile { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 角色
        /// </summary>
        public List<string> Roles { get; set; } = new List<string>();
    }
}
