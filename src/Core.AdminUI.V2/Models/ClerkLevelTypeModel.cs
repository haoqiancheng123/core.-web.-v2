﻿namespace Core.AdminUI.V2.Models
{
    public class ClerkLevelTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
