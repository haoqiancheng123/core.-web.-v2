﻿using System.Collections.Generic;

namespace Core.AdminUI.V2.Models.Menu
{
    public class MenuInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 控制后台
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string openType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string href { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string role { get; set; }
        public List<MenuInfo> children { get; set; } = new List<MenuInfo>();
    }
}
