﻿using System;

namespace Core.AdminUI.V2.Models.Config
{
    public class ReqMoney
    {
        public Guid  Id { get; set; }

        public int Money { get; set; }
    }
}
