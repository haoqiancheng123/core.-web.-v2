﻿using System;

namespace Core.AdminUI.V2.Models.Clerk
{
    /// <summary>
    /// 修改店员申请
    /// </summary>
    public class ReqApplyClerk
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 头像 
        /// </summary>
        public string ImageUrl { get; set; } 
        /// <summary>
        /// 图片  ; 分割
        /// </summary>
        public string Pictures { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 微信号
        /// </summary>
        public string WeChatAccount { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 性别  1：男 0 ：女
        public int Sex { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// 星座
        /// </summary>
        public string Constellation { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 职业
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// 有无经验
        /// </summary>
        public bool IsUndergo { get; set; }
        /// <summary>
        /// 平台名称
        /// </summary>
        public string PlatformName { get; set; }
        /// <summary>
        /// 最高单笔金币
        /// </summary>
        public decimal MaxMoney { get; set; }
        /// <summary>
        /// 最高店员等级
        /// </summary>
        public string MaxLeaval { get; set; }
        /// <summary>
        /// 是否离职
        /// </summary>
        public bool IsLeaveOffice { get; set; }
        /// <summary>
        /// 背景图片
        /// </summary>
        public string BackgroundImage { get; set; } 
        /// <summary>
        /// 标签  ; 分割
        /// </summary>
        public string Tags { get; set; } 
        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; } 
        /// <summary>
        /// 声音
        /// </summary>
        public string VoiceUrl { get; set; } 
    }
}
