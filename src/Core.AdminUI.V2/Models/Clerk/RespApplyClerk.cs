﻿using Core.FrameWork.Models.Enum.Common;
using Core.FrameWork.Models.Enum.User;
using Core.FrameWork.Models.User;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;


namespace Core.AdminUI.V2.Models.Clerk
{
    public class RespApplyClerk
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public Guid Id { get; set; } = Guid.Empty;

        /// <summary>
        /// 用户组Id
        /// </summary>
        public Guid GroupId { get; set; } = Guid.Empty;
        /// <summary>
        /// 用户组名称
        /// </summary>
        public string GroupName { get; set; } = string.Empty;
        /// <summary>
        /// 用户编号
        /// </summary>
        public int UserNumber { get; set; } = 0;
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; } = string.Empty;

        /// <summary>
        /// 店员昵称
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 微信号
        /// </summary>
        public string WeChatAccount { get; set; } = string.Empty;
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 性别  1：男 0 ：女
        /// </summary>
        public int Sex { get; set; } = 0;
        /// <summary>
        /// 有无经验
        /// </summary>
        public bool IsUndergo { get; set; } = false;
        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 最后修改时间
        /// </summary>
        public  DateTime LastModifyTime { get; set; } 
        /// <summary>
        /// 审核状态
        /// </summary>
        public ApplyClerkUser State { get; set; } = ApplyClerkUser.Review;
    }
}
