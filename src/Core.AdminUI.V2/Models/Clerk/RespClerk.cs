﻿using Core.FrameWork.Models.Enum.Common;
using Core.FrameWork.Models.Enum.User;
using Core.FrameWork.Models.User;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.AdminUI.V2.Models.Clerk
{
    public class RespClerk
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public Guid Id { get; set; } = Guid.Empty;
        /// <summary>
        /// 用户组
        /// </summary>
        public Guid Group { get; set; } = Guid.Empty;
        /// <summary>
        /// 用户编号 顺序
        /// </summary>
        public int UserNumber { get; set; }
        public int CharmNumber { get; set; }
        public string VipName { get; set; } = string.Empty;
        /// <summary>
        /// 店员昵称
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; } = string.Empty;
        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; } = string.Empty;
        /// <summary>
        /// 头像昵称
        /// </summary>
        public string ImageUrl { get; set; } = string.Empty;
        /// <summary>
        /// 状态
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public ApplyClerkUser State { get; set; } = ApplyClerkUser.Review;
    }
}
