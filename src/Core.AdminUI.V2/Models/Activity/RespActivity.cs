﻿using Core.FrameWork.Models.Enum.Common;
using Core.FrameWork.Models.User;
using System;

namespace Core.AdminUI.V2.Models.Activity
{
    public class RespActivity
    {
        /// <summary>
        /// 动态唯一标识
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 用户组
        /// </summary>
        public Guid GroupId { get; set; } 
        /// <summary>
        /// 用户编号
        /// </summary>
        public int UserNumber { get; set; }
        /// <summary>
        /// 店员等级
        /// </summary>
        public string  ClerkLevelTypeName { get; set; }
        /// <summary>
        /// 店员昵称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Abstract { get; set; }
        /// <summary>
        /// 图片  ;分割
        /// </summary>
        public string ImgUrl { get; set; }
        /// <summary>
        /// 点赞数量
        /// </summary>
        public int LikeNum { get; set; }
        /// <summary>
        /// 分享数量
        /// </summary>
        public int ShareNum { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime LastModifyTime { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 动态审核时间
        /// </summary>
        public DateTime ApproverTime { get; set; } 
    }
}
