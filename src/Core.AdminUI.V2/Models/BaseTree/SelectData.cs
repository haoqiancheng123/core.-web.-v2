﻿namespace Core.AdminUI.V2.Models.BaseTree
{
    public class SelectData
    {
        public string name { get; set; }
        public string value { get; set; }
        public string type { get; set; }
    }
}
