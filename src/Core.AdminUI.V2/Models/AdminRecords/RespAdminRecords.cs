﻿using System;

namespace Core.AdminUI.V2.Models.AdminRecords
{
    public class RespAdminRecords
    {
        /// <summary>
        /// 操作名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
