﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.AdminUI.V2.Models.AdminRecords
{
    public class RespAdminRecordsLogs
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 操作人姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 客户端ip
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// 请求路径
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 操作名称
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// 请求类型
        /// </summary>
        public string RequestMethod { get; set; }
        /// <summary>
        /// 响应时间
        /// </summary>
        public long ResponseTime { get; set; }
        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 序列化的入参信息
        /// </summary>
        public string RequestParamsString { get; set; }
        /// <summary>
        /// 序列化的响应信息
        /// </summary>
        public string ResponseData { get; set; }
        /// <summary>
        /// 客户端信息
        /// </summary>
        public string Agent { get; set; } = string.Empty;
    }
}
