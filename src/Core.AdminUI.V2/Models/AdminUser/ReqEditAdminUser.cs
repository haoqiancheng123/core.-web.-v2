﻿using System;

namespace Core.AdminUI.V2.Models
{
    public class ReqAddAdminUser
    {
 
        /// <summary>
        /// 用户名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string TrueName { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; } 
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string Roles { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

    }
    public class ReqEditAdminUser: ReqAddAdminUser
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid Id { get; set; } = Guid.Empty;
    }
}
