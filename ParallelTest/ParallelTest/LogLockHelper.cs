﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ParallelTest
{
    /// <summary>
    /// 日志锁
    /// </summary>
    public class LogLockHelper
    {
        
        static Dictionary<string, string[]> keys = new Dictionary<string, string[]>();
        static ReaderWriterLockSlim LogWriteLock = new ReaderWriterLockSlim();
        static int WritedCount = 0;
        static int FailedCount = 0;
        static string _contentRoot = @"C:\Users\qianc\source\repos\core.-web.-v2\ParallelTest\ParallelTest";

        public static void OutSql2Log(string rootPath, string filename, string[] dataParas, bool IsHeader = true)
        {
            try
            {
                //设置读写锁为写入模式独占资源，其他写入请求需要等待本次写入结束之后才能继续写入
                //注意：长时间持有读线程锁或写线程锁会使其他线程发生饥饿 (starve)。 为了得到最好的性能，需要考虑重新构造应用程序以将写访问的持续时间减少到最小。
                //      从性能方面考虑，请求进入写入模式应该紧跟文件操作之前，在此处进入写入模式仅是为了降低代码复杂度
                //      因进入与退出写入模式应在同一个try finally语句块内，所以在请求进入写入模式之前不能触发异常，否则释放次数大于请求次数将会触发异常
                LogWriteLock.EnterWriteLock();

                WriteLog(rootPath, filename, dataParas, IsHeader);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                FailedCount++;
            }
            finally
            {
                //退出写入模式，释放资源占用
                //注意：一次请求对应一次释放
                //      若释放次数大于请求次数将会触发异常[写入锁定未经保持即被释放]
                //      若请求处理完成后未释放将会触发异常[此模式不下允许以递归方式获取写入锁定]
                LogWriteLock.ExitWriteLock();
            }
        }
        public static void OutSql2LogQueue(string rootPath, string filename, string[] dataParas, bool IsHeader = true)
        {
            try
            {
                //设置读写锁为写入模式独占资源，其他写入请求需要等待本次写入结束之后才能继续写入
                //注意：长时间持有读线程锁或写线程锁会使其他线程发生饥饿 (starve)。 为了得到最好的性能，需要考虑重新构造应用程序以将写访问的持续时间减少到最小。
                //      从性能方面考虑，请求进入写入模式应该紧跟文件操作之前，在此处进入写入模式仅是为了降低代码复杂度
                //      因进入与退出写入模式应在同一个try finally语句块内，所以在请求进入写入模式之前不能触发异常，否则释放次数大于请求次数将会触发异常
                LogWriteLock.EnterWriteLock();

                List<string> str = new List<string>();

                var file = keys.Where(s => s.Key.Contains(rootPath));

                if (file.Count() == 5)
                {
                    foreach (var item in file)
                    {
                        keys.Remove(item.Key);
                        str.AddRange(item.Value);
                    }


                  


                    WriteLog(rootPath, filename, str.ToArray(), IsHeader);
                }
                else
                {
                    keys.Add(filename+ file.Count(), dataParas);
                }

             
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                FailedCount++;
            }
            finally
            {
                //退出写入模式，释放资源占用
                //注意：一次请求对应一次释放
                //      若释放次数大于请求次数将会触发异常[写入锁定未经保持即被释放]
                //      若请求处理完成后未释放将会触发异常[此模式不下允许以递归方式获取写入锁定]
                LogWriteLock.ExitWriteLock();
            }
        }
        private static void WriteLog(string rootPath, string filename, string[] dataParas, bool IsHeader = true)
        {
            var path = Path.Combine(_contentRoot, "App_Data/" + rootPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string logFilePath = Path.Combine(path, $@"{filename}.log");

            var now = DateTime.Now;
            string logContent = String.Join("\r\n", dataParas);
            if (IsHeader)
            {
                logContent = (
                   "--------------------------------\r\n" +
                   DateTime.Now + "|\r\n" +
                   String.Join("\r\n", dataParas) + "\r\n"
                   );
            }

            File.AppendAllText(logFilePath, logContent);
            WritedCount++;
        }
    }
    public class Dto
    {
        public string filename { get; set; }

        public string[] dataParas { get; set; }
    }

}
