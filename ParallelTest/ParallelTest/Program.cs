﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            //for (int i = 0; i < 100000; i++)
            //{
            //    Parallel.For(0, 1, i =>
            //    {
            //        LogLockHelper.OutSql2Log("RequestIpInfoLog", "RequestIpInfoLog" + DateTime.Now.ToString("yyyy-MM-dd"), new string[] { "123" + "," }, false);
            //    });
            //}
         
            //sw.Stop();
            //TimeSpan ts2 = sw.Elapsed;
            //Console.WriteLine("Parallel.For总共花费{0}ms.", ts2.TotalMilliseconds);


            Stopwatch sw_Eq = new Stopwatch();
            sw_Eq.Start();
            for (int i = 0; i < 1000000; i++)
            {
                LogLockHelper.OutSql2LogQueue("Test1", "Test1" + DateTime.Now.ToString("yyyy-MM-dd"), new string[] { "123" + "," }, false);
                LogLockHelper.OutSql2LogQueue("Test2", "Test2" + DateTime.Now.ToString("yyyy-MM-dd"), new string[] { "123" + "," }, false);
            }
            sw_Eq.Stop();
            TimeSpan tssw_Eq = sw_Eq.Elapsed;
            Console.WriteLine("for总共花费{0}ms.", tssw_Eq.TotalMilliseconds);
            Console.ReadKey();

        }
    }
}
